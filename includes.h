#ifndef __INCLUDE_H__
#define __INCLUDE_H__
//standard c lib headers
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <stdarg.h>
#include <stdint.h>
#include <ctype.h>
#include <math.h>
#include <stddef.h>
#include "TypeDefi.h"

//compiler headers
#include "Util.h"

//board config header
#include "gpio.h"

//shell
//#include "shell.h"
//#include "cmd_tables.h"
//#include "cmd_iwpriv.h"
//#include "cmd_exit.h"
//#include "cmd_help.h"
//#include "cmd_reboot.h"
//#include "cmd_ifconfig.h"
//#include "cmd_debug.h"
//#include "cmd_sysopt.h"
//#include "cmd_config.h"

//communication
//#include "Protocol.h"
#include "list.h"
#include "Comm.h"
#include "Cfg.h"

//app headers
#include "Timer.h"
#include "Handle.h"
#include "Motor.h"
#include "SysMgmt.h"
#include "version.h"


#endif