#ifndef __COMM_H
#define __COMM_H

#define dim(x) 				(sizeof(x) / sizeof(x[0]))   /* Returns number of elements	*/
#define ARRAY_SIZE(x)    	(sizeof(x) / sizeof((x)[0]))

typedef void(*PFNCT_PUT)(uint8_t*, uint16_t);

#define ACS_RX_DATA_SIZE				128
#define ACS_TX_DATA_SIZE				128
#define ACS_PKG_SIZE					256

//commands definition list
#define ACS_SYS_ON_OFF					0x10
#define ACS_SPD_LEVEL					0x11
#define ACS_SPEED_RATIO					0x12
#define ACS_SCROLL						0x13
#define ACS_SET_ADC						0x14
#define ACS_ENTER_ADC					0x15
#define ACS_GET_POINT					0x16
#define ACS_ADJUST						0x17
#define ACS_SPEAKER_ON					0x18
#define ACS_MAX_PWM_SET					0x19
#define ACS_MOTOR_OUT_DIS_EN			0x20

#define ACS_BATT_CAPACITY				0x50
#define ACS_ERROR_CODE					0x51
#define ACS_ADJUST_DONE					0x52
#define ACS_M_ALL_PARA_UPDATE			0x53
#define ACS_ENTER_ENG_MODE				0x54

typedef struct {
	uint8_t ver;
	uint8_t sopSize;
	uint8_t verSize;
	uint8_t cmdSize;
	uint8_t dataLenSize;
	uint8_t datacrcSize;
} acs_patten_t;

typedef enum {
	ACS_SOP = 0,
	ACS_VER,
	ACS_CMD_ID,
	ACS_DATA_LEN,
	ACS_DATA_CRC,
	ACS_DATA
} acs_state_t;

#define COMM_SOP					0xA5
#define PROTO_VER_0x01				0x01

typedef struct {
	uint8_t  sop;
	uint8_t  ver;
	uint8_t  command;
	uint8_t  datalen;
	uint8_t  datacrc;
}acs_patten_v1;
typedef acs_patten_v1 acs_header_t;

typedef struct {
	uint16_t is_busy : 			1;
	uint16_t was_resp_got : 	1;
    uint16_t parser_success :   1;
	uint16_t comm_excep :   	1;
} acs_control_t;

typedef struct {
	acs_control_t acs_ctrl;
	acs_header_t  header;
	acs_state_t   stateStep;
	uint16_t      size;
	uint16_t      pos;
	uint16_t	  txdatalen;
	uint16_t:     16;
	uint8_t       rxdata[ACS_RX_DATA_SIZE];
	uint8_t       txdata[ACS_TX_DATA_SIZE];
	uint8_t       txpkt[ACS_PKG_SIZE];
	PFNCT_PUT     pf_output;
	uint8_t  	  retry;
	uint8_t:	  8;
	uint16_t 	  timeout;
}acs_t;
extern __no_init acs_t InstanceDashboard;

typedef void(*PFNCT_ACS)(acs_t*);
typedef struct {
	uint8_t CmdCode;
	PFNCT_ACS pf_func;
	PFNCT_ACS pf_respfunc;
}CmdNode;
extern const uint16_t CmdTotal;
extern const CmdNode  CmdTable[];

typedef struct {
	struct list_head list;
	uint16_t length;
	uint8_t* dataPtr;
} CmdListNode_t;

void acs_Init( acs_t* instance );
void acs_WithDashboardInit( void );
acs_patten_t* acs_GetVerPatten( uint8_t ver );
bool acs_ParsingByte( acs_t* instance, uint8_t inbyte );
void acs_BuildPkt( acs_t* instance );
uint8_t acs_SearchCmd( uint8_t cmd, PFNCT_ACS* func, PFNCT_ACS* respfunc );
void acs_SalveRcvAction( acs_t* instance );
void acs_HostRespAction( acs_t* instance );
void acs_Host2Dashboard( acs_t* instance, uint8_t cmd );

void Comm_WithDashboardTask( void );
void Comm_WithStudioTask( void );

#endif /* #ifndef __COMM_H */
