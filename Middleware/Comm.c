/*******************************************************************************

(C) Copyright 2015, Chaoyi Dai. 	 All rights reserved

---------------- Important Notes -----------------------------------------------


----------------- Rivision History ---------------------------------------------
Version: V0.01
Author: Chaoyi Dai
Last Modify Date: 2015/10/25
Discription: Initial version

*******************************************************************************/

#include "includes.h"
#include "adc.h"
#include "stm32f1xx_hal.h"
#include "tim.h"
#include "usart.h"


const acs_patten_t acs_pattens[] = {
	{.ver=0x01, .sopSize=1, .verSize=1, .cmdSize=1, .dataLenSize=1, .datacrcSize=1},
};

__no_init acs_t InstanceDashboard;


void acs_Init( acs_t* instance )
{
	if( instance != NULL ) {
		memset( &(instance->header), 0x0, sizeof(acs_header_t) );
		instance->acs_ctrl.parser_success = false;
		instance->pos  = 0;
		instance->size = 0;
		instance->stateStep = ACS_SOP;
	}
}

void acs_WithDashboardInit( void )
{
    acs_t* acs = &InstanceDashboard;
    acs_Init( acs );
    acs->pf_output = (PFNCT_PUT)UART3_Puts;
}

acs_patten_t* acs_GetVerPatten( uint8_t ver )
{
	acs_patten_t* patten;
	uint8_t i;
    
	for( i=0; i<ARRAY_SIZE(acs_pattens); i++ ) {
		patten = (acs_patten_t *)&acs_pattens[i];
		if (ver == patten->ver) {
			return( patten );
		}
	}
	
	return( NULL );
}

bool acs_ParsingByte( acs_t* instance, uint8_t inbyte )
{
	uint8_t* header = (uint8_t*)&(instance->header);
	uint8_t  crc;
	
	switch( instance->stateStep ) {
		case ACS_SOP:
			instance->pos = 0;
			if( inbyte == COMM_SOP ) {
				instance->acs_ctrl.is_busy = true;
				header[instance->pos++] = inbyte;
				instance->stateStep++;
			}
			break;
		case ACS_VER:
			header[instance->pos++] = inbyte;
			instance->stateStep++;
			break;
		case ACS_CMD_ID:
			header[instance->pos++] = inbyte;
			instance->stateStep++;
			break;
		case ACS_DATA_LEN:
			header[instance->pos++] = inbyte;
			if( instance->header.datalen == 0 ) {
				instance->stateStep = ACS_SOP;
				return( true );
			}
			else {
				instance->stateStep++;
			}
			break;
		case ACS_DATA_CRC:
			header[instance->pos++] = inbyte;
			instance->size = instance->header.datalen;
			instance->pos  = 0;
			instance->stateStep++;
			break;
		case ACS_DATA:
			instance->rxdata[instance->pos++] = inbyte;
			if( --instance->size == 0 ) {
				crc = crc8(instance->rxdata, instance->header.datalen);
				if( crc == instance->header.datacrc ) {
					instance->stateStep = ACS_SOP;
					return( true );
				}
				else {
					Utl_MemSet( (uint8_t*)instance, 0, sizeof(acs_t) );
					instance->stateStep = ACS_SOP;
				}
			}
			break;
		default:
			Utl_MemSet( (uint8_t*)instance, 0, sizeof(acs_t) );
			instance->stateStep = ACS_SOP;
			break;
	}
	return( false );
}

void acs_BuildPkt( acs_t* instance )
{
	uint16_t i = 0;
	uint16_t len  = instance->txdatalen;
	uint8_t* data = instance->txdata;
	uint8_t* buf  = instance->txpkt;
	
	Utl_MemSet( buf, 0x0, ACS_PKG_SIZE );
	buf[i++] = COMM_SOP;
	buf[i++] = PROTO_VER_0x01;
	buf[i++] = instance->header.command;
	buf[i++] = len;
	if( len > 0 ) {
		buf[i++] = crc8(data, len);
		Utl_MemCopy( &(buf[i]), data, len );
		i += len;
	}
    if( i > ACS_PKG_SIZE ) {
        i = ACS_PKG_SIZE;
    }
    if( instance->pf_output != (PFNCT_PUT)NULL ) {
        instance->pf_output( buf, i );
    }
}

/* return 1: support, 0: not support */
uint8_t acs_SearchCmd( uint8_t cmd, PFNCT_ACS* func, PFNCT_ACS* respfunc )
{
	uint16_t i;
	CmdNode* tptr;
	
	/* Search command set */
	tptr = (CmdNode*)CmdTable;
	for( i=0; i<CmdTotal; i++ ) {
		if( cmd == (tptr+i)->CmdCode ) {
			*func = (PFNCT_ACS)(tptr+i)->pf_func;
			*respfunc = (PFNCT_ACS)(tptr+i)->pf_respfunc;
			return( true );
		}
	}
	return( false );
}

void acs_SalveRcvAction( acs_t* instance )
{
	bool dashboardrsp = false;
	PFNCT_ACS func  = NULL;
	PFNCT_ACS respf = NULL;
	
	if( acs_SearchCmd(instance->header.command, &func, &respf) == 0 ) {
		return;
	}
	switch( instance->header.command ) {
		case ACS_BATT_CAPACITY:
		case ACS_ADJUST_DONE:
		case ACS_ERROR_CODE:
			dashboardrsp = true;
			break;
		default:
			dashboardrsp = false;
			break;
	}
	
	if( dashboardrsp == false ) {
		if( func != NULL ) {
			func( instance );
		}
	}
	
	instance->txdatalen = 0;
	if( respf != NULL ) {
		respf( instance );
	}
	if( dashboardrsp == false ) {
		acs_BuildPkt( instance );
	}
}

void acs_HostRespAction( acs_t* instance )
{
	PFNCT_ACS func  = NULL;
	PFNCT_ACS respf = NULL;
	
	if( acs_SearchCmd(instance->header.command, &func, &respf) == 0 ) {
		return;
	}
	instance->txdatalen = 0;
	if( respf != NULL ) {
		respf( instance );
	}
	//acs_BuildPkt( instance );
}

void acs_Host2Dashboard( acs_t* instance, uint8_t cmd )
{
	PFNCT_ACS hostfunc = NULL;
	PFNCT_ACS respfunc = NULL;
	
	if( instance->acs_ctrl.is_busy == true ) {
		return;
	}
	instance->header.command = cmd;
	if( acs_SearchCmd(instance->header.command, &hostfunc, &respfunc) == 0 ) {
		return;
	}
	instance->txdatalen = 0;
	if( hostfunc != NULL ) {
		hostfunc( instance );
	}
	acs_BuildPkt( instance );
}

static void acs_SysOnOff( acs_t* instance )
{
	SysMgmt.Status.power_on = (instance->rxdata[0] == 0x01) ? (false) : (true);
}

static void acs_SpdLevelSetting( acs_t* instance )
{
	uint16_t i = 0;
	__motor_t* skt = &Motor;
    skt->speedlevel = instance->rxdata[i++];
	if( skt->speedlevel > 0 ) {
		skt->speedlevel--;
	}
}

static void acs_SpdRadioSetting( acs_t* instance )
{
	uint16_t i = 0;
	uint16_t speedratio = 0;
	
	__motor_t* skt = &Motor;
    speedratio = instance->rxdata[i++];
	if( (abs(speedratio) >= 80) && (abs(speedratio) <= 100) ) {
		skt->speedratio = speedratio;
	}
}

static void acs_ScrollSetting( acs_t* instance )
{
	uint16_t i = 0;
	__motor_t* skt = &Motor;
    skt->scroll = instance->rxdata[i++];
}

static void acs_HandleAdcRangeSetting( acs_t* instance )
{
	uint16_t i = 0;
	int16_t temp = 0;
	handle_t* skt = &Handle;
	
	temp  = instance->rxdata[i++] << 8;
	temp |= instance->rxdata[i++];
	if( temp & 0x8000 ) {
		temp &= 0x7fff;
		skt->p_y_front = -temp;
	}
	else {
		skt->p_y_front = temp;
	}
	
	temp  = 0;
	temp  = instance->rxdata[i++] << 8;
	temp |= instance->rxdata[i++];
	if( temp & 0x8000 ) {
		temp &= 0x7fff;
		skt->p_y_back = -temp;
	}
	else {
		skt->p_y_back = temp;
	}
	
	temp  = 0;
	temp  = instance->rxdata[i++] << 8;
	temp |= instance->rxdata[i++];
	if( temp & 0x8000 ) {
		temp &= 0x7fff;
		skt->p_x_left = -temp;
	}
	else {
		skt->p_x_left = temp;
	}
	
	temp  = 0;
	temp  = instance->rxdata[i++] << 8;
	temp |= instance->rxdata[i++];
	if( temp & 0x8000 ) {
		temp &= 0x7fff;
		skt->p_x_right = -temp;
	}
	else {
		skt->p_x_right = temp;
	}
	
	temp  = 0;
	temp  = instance->rxdata[i++] << 8;
	temp |= instance->rxdata[i++];
	skt->adc_x0 = temp;
	
	temp  = 0;
	temp  = instance->rxdata[i++] << 8;
	temp |= instance->rxdata[i++];
	skt->adc_y0 = temp;
}

static void acs_EnterHandleAdcEng( acs_t* instance )
{
	sysmgmt_t* mgmt = &SysMgmt;
	mgmt->Status.enter_handle_eng = (instance->rxdata[0] == 0) ? true : false;
}

static void acs_GetPointAdcResp( acs_t* instance )
{
	uint8_t  samples;
	uint16_t len = 0;
	uint16_t adc = 0;
	
	samples = 8;
	while( samples-- ) {
		adc = Fifo_Proc( &fifo_handle_x, Get_adc(ADC_CHANNEL_8) );
	}
	instance->txdata[len++] = (uint8_t)(adc >> 8);
	instance->txdata[len++] = (uint8_t)(adc >> 0);
	
	samples = 8;
	while( samples-- ) {
		adc = Fifo_Proc( &fifo_handle_y, Get_adc(ADC_CHANNEL_9) );
	}
	instance->txdata[len++] = (uint8_t)(adc >> 8);
	instance->txdata[len++] = (uint8_t)(adc >> 0);
	instance->txdatalen = len;
}

static void acs_SpdAdjustStart( acs_t* instance )
{
	sysmgmt_t* mgmt = &SysMgmt;
	mgmt->Status.enter_spd_calib_eng = true;
	mgmt->Status.spd_calib_start = true;
}

static void acs_SpeakerOn( acs_t* instance )
{
	sysmgmt_t* mgmt = &SysMgmt;
	mgmt->Status.speaker_on = (instance->rxdata[0] == 0) ? true : false;
	SysMgmt_ExpBuzzer( mgmt );
}

static void acs_BattCapacity( acs_t* instance )
{
	uint16_t len = 0;
	sysmgmt_t* mgmt = &SysMgmt;
	
	instance->txdata[len++] = (uint8_t)(mgmt->PowerCapacity);
	instance->txdatalen = len;
}

static void acs_BattCapacityResp( acs_t* instance )
{
	sysmgmt_t* mgmt = &SysMgmt;
	mgmt->CommTimeout = 5;
}

static void acs_ErrorCode( acs_t* instance )
{
	uint16_t len = 0;
	sysmgmt_t* mgmt = &SysMgmt;
	
	instance->txdata[len++] = (uint8_t)(mgmt->sysExcep.ErrCode >> 24);
	instance->txdata[len++] = (uint8_t)(mgmt->sysExcep.ErrCode >> 16);
	instance->txdata[len++] = (uint8_t)(mgmt->sysExcep.ErrCode >> 8);
	instance->txdata[len++] = (uint8_t)(mgmt->sysExcep.ErrCode >> 0);
	instance->txdatalen = len;
}

static void acs_SpdAdjustDone( acs_t* instance )
{
	uint8_t  i;
	uint16_t len = 0;
	__motor_t* skt = &Motor;
	for( i=0; i<SPD_LEVELS; i++ ) {
		instance->txdata[len++] = (uint8_t)(skt->maxpwm[SPD_LEVELS - 1 - i] >> 8);
		instance->txdata[len++] = (uint8_t)(skt->maxpwm[SPD_LEVELS - 1 - i] >> 0);
	}
	instance->txdatalen = len;
}

static void acs_SpdAdjustDoneResp( acs_t* instance )
{
	uint8_t temp = 0;
	
	temp = temp;
}

static void acs_MaxPwmSet( acs_t* instance )
{
	uint8_t  i;
	uint16_t offset = 0;
	uint16_t temp = 0;
	__motor_t* skt = &Motor;
    
	for( i=0; i<SPD_LEVELS; i++ ) {
		temp  = instance->rxdata[offset++] << 8;
		temp |= instance->rxdata[offset++];
		skt->maxpwm[SPD_LEVELS - 1 - i] = temp;
	}
	//skt->speed_k = instance->rxdata[offset++];
	SysMgmt.Status.para_got = true;
}

static void acs_MotorOutDisEn( acs_t* instance )
{
	__motor_t* motor = &Motor;
	motor->Status_bit.motor_out_en = ((instance->rxdata[0] == 0) ? (false) : (true));
}

static void acs_motorallPara( acs_t* instance )
{
	uint8_t  mdir = 0;
	uint16_t i = 0;
	int16_t temp = 0;
	__motor_t* motor = &Motor;
	handle_t* handle = &Handle;
	
	SysMgmt.Status.power_on = (instance->rxdata[i++] == 0x01) ? (false) : (true);
    motor->speedlevel = instance->rxdata[i++];
	if( motor->speedlevel > 0 ) {
		motor->speedlevel--;
	}
	motor->speedratio = instance->rxdata[i++];
	motor->scroll = instance->rxdata[i++];
	
	temp  = instance->rxdata[i++] << 8;
	temp |= instance->rxdata[i++];
	if( temp & 0x8000 ) {
		temp &= 0x7fff;
		handle->p_y_front = -temp;
	}
	else {
		handle->p_y_front = temp;
	}
	
	temp  = 0;
	temp  = instance->rxdata[i++] << 8;
	temp |= instance->rxdata[i++];
	if( temp & 0x8000 ) {
		temp &= 0x7fff;
		handle->p_y_back = -temp;
	}
	else {
		handle->p_y_back = temp;
	}
	
	temp  = 0;
	temp  = instance->rxdata[i++] << 8;
	temp |= instance->rxdata[i++];
	if( temp & 0x8000 ) {
		temp &= 0x7fff;
		handle->p_x_left = -temp;
	}
	else {
		handle->p_x_left = temp;
	}
	
	temp  = 0;
	temp  = instance->rxdata[i++] << 8;
	temp |= instance->rxdata[i++];
	if( temp & 0x8000 ) {
		temp &= 0x7fff;
		handle->p_x_right = -temp;
	}
	else {
		handle->p_x_right = temp;
	}
	
	temp  = 0;
	temp  = instance->rxdata[i++] << 8;
	temp |= instance->rxdata[i++];
	handle->adc_x0 = temp;
	
	temp  = 0;
	temp  = instance->rxdata[i++] << 8;
	temp |= instance->rxdata[i++];
	handle->adc_y0 = temp;
	
	for( i=0; i<SPD_LEVELS; i++ ) {
		temp  = instance->rxdata[i++] << 8;
		temp |= instance->rxdata[i++];
		motor->maxpwm[SPD_LEVELS - 1 - i] = temp;
	}
	mdir = instance->rxdata[i++];
	motor->Status_bit.l_motor_front = (((mdir & 0x01) == 0) ? (true) : (false));
	motor->Status_bit.r_motor_front = (((mdir & 0x02) == 0) ? (true) : (false));
	motor->speed_k = instance->rxdata[i++];
	SysMgmt.Status.para_got = true;
}

/* Command set table */
const CmdNode CmdTable[] = {
	ACS_SYS_ON_OFF,			acs_SysOnOff,				NULL,
	ACS_SPD_LEVEL,			acs_SpdLevelSetting,		NULL,
	ACS_SPEED_RATIO,		acs_SpdRadioSetting,		NULL,
	ACS_SCROLL,				acs_ScrollSetting,			NULL,
	ACS_SET_ADC,			acs_HandleAdcRangeSetting,	NULL,
	ACS_ENTER_ADC,			acs_EnterHandleAdcEng,		NULL,
	ACS_GET_POINT,			NULL,						acs_GetPointAdcResp,
	ACS_ADJUST,				acs_SpdAdjustStart,			NULL,
	ACS_SPEAKER_ON,			acs_SpeakerOn,				NULL,
	ACS_MAX_PWM_SET,		acs_MaxPwmSet,				NULL,
	ACS_MOTOR_OUT_DIS_EN,	acs_MotorOutDisEn,			NULL,
	
	ACS_BATT_CAPACITY,		acs_BattCapacity,			acs_BattCapacityResp,
	ACS_ERROR_CODE,			acs_ErrorCode,				NULL,
	ACS_ADJUST_DONE,		acs_SpdAdjustDone,			acs_SpdAdjustDoneResp,
	ACS_M_ALL_PARA_UPDATE,	acs_motorallPara,			NULL,
	ACS_ENTER_ENG_MODE,		NULL,						NULL,
};
const uint16_t CmdTotal = dim(CmdTable);

void Comm_WithDashboardTask( void )
{
	uint8_t  inbyte;
	uint16_t rxlen;
	acs_t*   instance = &InstanceDashboard;
	
	rxlen = UART_GetRxBuffSize( &huart3 );
	while( rxlen-- ) {
		UART_Gets( &huart3, &inbyte, 1 );
		if( acs_ParsingByte(instance, inbyte) == true ) {
			acs_SalveRcvAction( instance );
			instance->acs_ctrl.was_resp_got = true;
			instance->acs_ctrl.comm_excep   = false;
			instance->acs_ctrl.is_busy 		= false;
		}
	}
}

void Comm_WithStudioTask( void )
{
	
}
