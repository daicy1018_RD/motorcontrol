#ifndef __TM_UTILS_H__
#define __TM_UTILS_H__

void TM_Dly10us( INT16U u16_us );
void TM_Dlyms( INT16U u16_ms );

#endif /* #ifndef __TM_UTILS_H__ */
