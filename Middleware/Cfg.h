#ifndef __CFG_H
#define __CFG_H

#define APP_CFG_BASE_ADDR     		0x0800E000
#define APP_CFG_END_ADDR      		0x0800FFFF

typedef enum {
	IX_INIT = 0,
	
	IX_SCROLL
}Ix_Info;

#define INFO_BASEADDR    			APP_CFG_BASE_ADDR
#define INIT_FLAG      				(0xA5A5 + 0)

#define HANDLE_CENTER_ADCX			2048
#define HANDLE_CENTER_ADCY			2048

#define HANDLE_ADCX_MAX				4095
#define HANDLE_ADCY_MAX				4095

#define HANDLE_ADCX_MIN				0
#define HANDLE_ADCY_MIN				0

#define C_SCROLL        			2395		//(46.85+3.2)*3.14159 = 168mm x 10

typedef struct {
    uint32_t int_buzzer_disable:		1;
	uint32_t left_motor_dir:			1;		//0: foreward; 1:backward
	uint32_t right_motor_dir:			1;
	uint32_t LR_motor_switch:			1;		//0: 
	uint32_t breaker_disable:			1;
	
} __syscfg_bits;

/* System Cfg Info Area Definition */
typedef struct System_Cfg SYS_CFG;
struct System_Cfg {
	uint16_t u16_InitFlg;
	__syscfg_bits sysStatusPara;
	
	uint16_t u16_Scroll;
	
	uint32_t hallposX0;
    uint32_t hallposY0;
    uint32_t hallposXMax;
    uint32_t hallposYMax;
	uint32_t hallposXMin;
    uint32_t hallposYMin;
	
	int32_t  pwml_Max;
	int32_t  pwmr_Max;
};
__no_init extern SYS_CFG st_Cfg;

bool Cfg_init( void );
void Cfg_BlkErase( uint16_t* addr );
void Cfg_Wr( uint16_t* addr, uint16_t* buff, uint16_t len );
void Cfg_Rd( uint16_t* addr, uint16_t* buff, uint16_t u16_len );

#endif
