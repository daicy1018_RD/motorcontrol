/*******************************************************************************

   (C) Copyright 2015/10/25, Chaoyi Dai. 	 All rights reserved

----------------- File Information ---------------------------------------------
       File Name: Timer.c
          Author: Chaoyi Dai
Last Modify Date: 2015/10/25
     Discription:

---------------- Important Notes -----------------------------------------------


----------------- Rivision History ---------------------------------------------
         Version:
          Author: Chaoyi Dai
Last Modify Date: 2015/10/25
     Discription: Initial version

*******************************************************************************/

#include "includes.h"


TimerFlag_t TimerFlag;
static uint32_t tick = 0;

void SysTimerHandler( void )
{
    TimerFlag_t* timer = &TimerFlag;
	
    tick++;
	timer->Timer_1ms_Trig = true;
	if( (tick % TICK_5_ms) == 0 ) {            // 5ms
		timer->Timer_5ms_Trig = true;
		if( SysMgmt.Status.sys_init_done == true ) {
			Handle_PosiGet( &Handle );
			Motor_PwmCalc( &Motor, &Handle );
			Motor_PwmOutProc( &Motor );
		}
	}
	
	if( (tick % TICK_10_ms) == 0 ) {            // 10ms
		timer->Timer_10ms_Trig = true;
		Motor_PwmOutDlyTimer( &Motor );
    }
	
	if( (tick % TICK_20_ms) == 0 ) {            // 20ms
		timer->Timer_20ms_Trig = true;
    }
	
    if( (tick % TICK_50_ms) == 0 ) {            // 50ms
		timer->Timer_50ms_Trig = true;
    }
	
	if( (tick % TICK_100_ms) == 0 ) {           // 100ms
		timer->Timer_100ms_Trig = true;
    }
	
    if( (tick % TICK_500_ms) == 0 ) {           // 500ms
		st_Rpm.control.b_500ms  = true;
		timer->Timer_500ms_Trig = true;
    }
	
	if( (tick % TICK_1000_ms) == 0 ) {          // 1000ms
		tick = 0;
		timer->Timer_1s_Trig = true;
	}
	Comm_WithDashboardTask();
}

