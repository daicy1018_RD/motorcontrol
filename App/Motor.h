#ifndef MOTOR_H
#define	MOTOR_H

#ifdef __cplusplus
 extern "C" {
#endif

typedef enum {
	MOTOR_L = 0,
    MOTOR_R,
} motor_phase_t;

typedef enum {
	FORWARD = 0,
    BACKWARD,
    STOP,
} motor_act_t;

typedef enum {
	SPD_LEVEL_1 = 0,
    SPD_LEVEL_2,
    SPD_LEVEL_3,
    SPD_LEVEL_4,
	SPD_LEVEL_5,
	SPD_LEVELS,
} speed_level_t;

typedef enum {
	SCROLL_12 = 12,		//unit: inch
    SCROLL_16 = 16,
	SCROLL_18 = 18,
    SCROLL_22 = 22,
} scroll_t;

#define DUTY_MAX            		(4800 * 90 / 100)  	//95% cycle
#define DUTY_STOP           		50                 	//2% cycle

#define SPD1_DUTY_MAX				1920		//4800 * 40%
#define SPD2_DUTY_MAX				2400		//4800 * 50%
#define SPD3_DUTY_MAX				2880		//4800 * 60%
#define SPD4_DUTY_MAX				3600		//4800 * 75%
#define SPD5_DUTY_MAX				DUTY_MAX	//4800 * 90%
#define BACK_DUTY_MAX				(-2000)

typedef struct {
	uint32_t run_dir:         		1;
	uint32_t L_reach_m_target:      1;
	uint32_t R_reach_m_target:      1;
	uint32_t special_handle:      	1;
	uint32_t motor_pwm_out:      	1;
	uint32_t motor_action_dly:		1;
	uint32_t motor_stop_dly:		1;
	
	uint32_t l_motor_front:			1;
	uint32_t r_motor_front:			1;
	uint32_t motor_out_en:			1;
} __status_bits;

typedef struct {
	INT32U_BIT( Status, __status_bits );
	uint8_t  speedlevel;
	uint8_t  speedratio;	//base on right
	uint8_t  l_Send_Cycle;
	uint8_t  r_Send_Cycle;
	
	int32_t  pwml_Target;
	int32_t  pwmr_Target;
	int32_t  left_middle_Target;
	int32_t  right_middle_Target;
	
	int32_t  pwml_Current;
	int32_t  pwmr_Current;
	int32_t  pwml_Step;
	int32_t  pwmr_Step;
	
	uint16_t scroll;
	uint16_t maxpwm[SPD_LEVELS];
	
	uint16_t speed_k;
}__motor_t;
extern __no_init __motor_t Motor;

void Motor_Init( void );
uint16_t Motor_CalcSpeedK( uint16_t pwm );
void Motor_PwmCalc( __motor_t* motor, handle_t* handle );
void Motor_PwmOutControl( __motor_t* skt );
void Motor_PwmOutProc( __motor_t* skt );
void Motor_PwmOutDlyTimer( __motor_t* skt );


#define C_SPDMAX      		(1600+500)  	//unit: 0.01 km = 10000.0mm
#define C_SPDMIN      		50
#define C_HOUR_RPMMIN  		2583    			//(C_SPDMIN*100000/C_SCROLL_CF)
#define C_HOUR_RPMMAX  		108514  			//(C_SPDMAX*100000/C_SCROLL_CF)
#define C_PULSEMAX      	26757   			//58982400/C_HOUR_RPMMIN
#define C_PULSEMIN      	637     			//58982400/C_HOUR_RPMMAX

#define C_RPMLOWLIMIT		60   				//2sec = 60 x 32768us
#define C_SCROLL			2395//1540//1640				//(46.85+3.2)*3.14159 = 168mm x 10
#define C_SCRL_MAX			3000
#define C_SCRL_MIN			1000
#define C_PULSWMAX			1512000			//(2000000*3600)/((10-2)*1000000/1680) 	:1.00km/h
#define C_PULSWMIN			71153				//(2000000*3600)/((120+50)*1000000/1680)	:12.0km/h	// 16.0km/h :57600
#define C_RPMMAXCYCLE		625				//2.5sec = 4ms x 625
#define C_RPMSTPCNT			(C_RPMMAXCYCLE + 1)

#define RPM_OFFSET         10

typedef enum {
	C_KM = 0,
	C_MILE,
} __spd_mode_t;

typedef struct {
	uint16_t b_first:   		1;
	uint16_t b_stop:    		1;
	uint16_t b_ok:      		1;
	uint16_t b_stab:    		1;
	uint16_t b_500ms:   		1;
	uint16_t b_spd_mode:		1;
} __rpm_ctrl_t;

/* Rpm Handle Socket Definition */
typedef struct Rpm_Socket RPM;
struct Rpm_Socket {
	__rpm_ctrl_t control;
	uint16_t  rpmSpd;
	uint16_t  scroll;
	uint16_t  speed[3];
	uint32_t  rpmCnt;
	uint32_t  maxCnt;
	uint32_t  minCnt;
	uint32_t  ovrCnt;
};
extern RPM st_Rpm;

void Rpm_Init( void );
void Rpm_CntOvr( void );
void Rpm_CntLmt( uint16_t u16_hispd, uint16_t u16_lospd );
void Rpm_CalCnt( void );
void Rpm_Dcd( void );
void Rpm_ClrBuf( void );

#ifdef __cplusplus
}
#endif

#endif	/* MOTOR_H */
