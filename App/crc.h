#ifndef CRC_H
#define	CRC_H

unsigned short Crc16(unsigned char *byte, int len);
unsigned short UpdateCrc16(unsigned short crc, unsigned char byte);

#endif	/* CRC_H */

