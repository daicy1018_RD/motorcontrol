/*******************************************************************************

   (C) Copyright 2015/10/25, Chaoyi Dai. 	 All rights reserved

----------------- File Information ---------------------------------------------
       File Name: motor.c
          Author: Chaoyi Dai
Last Modify Date: 2016/1/2
     Discription:

---------------- Important Notes -----------------------------------------------


----------------- Rivision History ---------------------------------------------
         Version:
          Author: Chaoyi Dai
Last Modify Date: 2016/1/2
     Discription: Initial version

*******************************************************************************/

#include "includes.h"
#include "adc.h"
#include "stm32f1xx_hal.h"
#include "tim.h"


__no_init __motor_t Motor;

void Motor_Init( void )
{
	__motor_t* motor = &Motor;
	
    HAL_TIM_OC_Init( &htim1 );
    HAL_TIM_OC_Stop( &htim1, TIM_CHANNEL_1 );
    HAL_TIM_OC_Stop( &htim1, TIM_CHANNEL_2 );
    HAL_TIM_OC_Stop( &htim1, TIM_CHANNEL_3 );
    HAL_TIM_OC_Stop( &htim1, TIM_CHANNEL_4 );
	
	memset( motor, 0x00, sizeof(__motor_t) );
	//Handle_MotorActEn( false );
	motor->speedlevel = SPD_LEVEL_2;
	
	motor->maxpwm[0] = SPD1_DUTY_MAX;
	motor->maxpwm[1] = SPD2_DUTY_MAX;
	motor->maxpwm[2] = SPD3_DUTY_MAX;
	motor->maxpwm[3] = SPD4_DUTY_MAX;
	motor->maxpwm[4] = SPD5_DUTY_MAX;
	
	motor->speedratio = 100;
	motor->Status_bit.l_motor_front = true;
	motor->Status_bit.r_motor_front = true;
	motor->Status_bit.motor_out_en  = true;
	
	motor->speed_k = 100;
	
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_3, GPIO_PIN_RESET);	//R2 enable
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_4, GPIO_PIN_RESET);	//R1 enable
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_5, GPIO_PIN_RESET);	//L2 enable
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_6, GPIO_PIN_RESET);	//L1 enable
	
	HAL_TIM_PWM_Reset();
}

uint16_t Motor_CalcSpeedK( uint16_t pwm )
{
	uint64_t temp = pwm;
	temp  = (4225 * temp) * 1000 / (4096*4096);
	return( (uint16_t)temp );
}

/*
if Hy > Hy0 :
(1) left:  ((3.5*ADCy - 3.5*ADCy0 - ADCx + ADCx0)*552.5*2400) / (4096*4096)
(2) right: ((3.5*ADCy - 3.5*ADCy0 + ADCx - ADCx0)*552.5*2400) / (4096*4096)
if Hy <= Hy0 :
(3) left:  ((2.0*ADCy - 2.0*ADCy0 - ADCx + ADCx0)*552.5*2400) / (4096*4096)
(4) right: ((2.0*ADCy - 2.0*ADCy0 + ADCx - ADCx0)*552.5*2400) / (4096*4096)

ADCy0 = ADCx0 = 4096 / 2 = 2048

SPD1_DUTY_MAX: 1080
SPD2_DUTY_MAX: 1350
SPD3_DUTY_MAX: 1620
SPD4_DUTY_MAX: 1890
SPD5_DUTY_MAX: 2160		//2400 * 0.9

K1 = (4225 * SPD1_DUTY_MAX) / (4096*4096) = 0.356;
K2 = (4225 * SPD2_DUTY_MAX) / (4096*4096) = 0.445;
K3 = (4225 * SPD3_DUTY_MAX) / (4096*4096) = 0.533;
K4 = (4225 * SPD4_DUTY_MAX) / (4096*4096) = 0.622;
K5 = (4225 * SPD5_DUTY_MAX) / (4096*4096) = 0.604;

Center: 4096 / 2 = 2048
Xoffset: +-50
Yoffset: +-50
*/

#define COUNT_OFFSET_X				(100 * 2)
#define COUNT_OFFSET_Y				(100 * 2)

#define DIVIDE     					120

#define DELAY_10TIME				2
#define DELAY_50TIME				10
#define DELAY_100TIME				20
static int32_t mr_t_step = 0;
static int32_t ml_t_step = 0;
static int32_t mr_c_step = 0;
static int32_t ml_c_step = 0;
static uint32_t mr_acc = 0;
static uint32_t ml_acc = 0;
static uint32_t delay  = 0;
void Motor_CalcStep( __motor_t* skt )
{
	int32_t r_delta   = 0;
	int32_t l_delta   = 0;
	int32_t r_p_dlt   = 0;
	int32_t l_p_dlt   = 0;
	int32_t temp      = 0;
	int32_t dly_time  = 0;
	int32_t l_target  = 0;
	int32_t r_target  = 0;
	int32_t l_current = 0;
	int32_t r_current = 0;
	
	l_target = skt->pwml_Target;
	r_target = skt->pwmr_Target;
	l_current = skt->pwml_Current;
	r_current = skt->pwmr_Current;
	if( skt->Status_bit.special_handle == true ) {
		if( skt->Status_bit.L_reach_m_target == false ) {
			l_target = skt->left_middle_Target;
		}
		if( skt->Status_bit.R_reach_m_target == false ) {
			r_target = skt->right_middle_Target;
		}
	}
	
	r_delta = abs(r_target - r_current);
	l_delta = abs(l_target - l_current);
	mr_t_step = r_delta/DIVIDE;
	ml_t_step = l_delta/DIVIDE;
	
//	mr_t_step = (mr_t_step > 10) ? (10) : (mr_t_step);
	mr_t_step = (mr_t_step < 1) ?  (1)  : (mr_t_step);
//	ml_t_step = (ml_t_step > 10) ? (10) : (ml_t_step);
	ml_t_step = (ml_t_step < 1) ?  (1)  : (ml_t_step);
	
	r_p_dlt = (r_delta * 100)/l_delta;
	l_p_dlt = (l_delta * 100)/r_delta;
	r_p_dlt = (r_p_dlt > 150) ? (150) : (r_p_dlt);
	r_p_dlt = (r_p_dlt < 1)   ?   (1) : (r_p_dlt);
	l_p_dlt = (l_p_dlt > 150) ? (150) : (l_p_dlt);
	l_p_dlt = (l_p_dlt < 1)   ?   (1) : (l_p_dlt);
	
	switch( skt->scroll ) {
		case SCROLL_22:
			dly_time = DELAY_50TIME + 5;
			break;
		default:
			dly_time = DELAY_50TIME;
			break;
	}
	if( (abs(l_target) < abs(l_current)) || (abs(r_target) < abs(r_current)) ) {
		dly_time = DELAY_10TIME;
	}
	
	delay++;
	if( delay >= dly_time ) {
		delay = 0;
		//left
		if( ml_c_step < ml_t_step ) {
			ml_acc += l_p_dlt;
		}
		else {
			if( ml_c_step > ml_t_step ) {
				if( ml_acc > l_p_dlt ) {
					ml_acc -= l_p_dlt;
				}
				else {
					ml_acc = 0;
				}
			}
		}
		ml_c_step = ml_acc / 100;
		//right
		if( mr_c_step < mr_t_step ) {
			mr_acc += r_p_dlt;
		}
		else {
			if( mr_c_step > mr_t_step ) {
				if( mr_acc > r_p_dlt ) {
					mr_acc -= r_p_dlt;
				}
				else {
					mr_acc = 0;
				}
			}
		}
		mr_c_step = mr_acc / 100;
	}
	
	if( abs(l_target) < abs(l_current) ) {
		switch( skt->scroll ) {
			case SCROLL_22:
				temp = ml_acc;
				break;
			default:
				temp = ml_acc + abs(l_current) * 20 / 100;
				if( temp < 300 ) {
					temp = 300;
				}
				break;
		}
	}
	else {
		temp = ml_acc;
	}
	temp /= 100;
	if( temp < 1 ) {
		temp = 1;
	}
	skt->pwml_Step = (l_target > l_current) ? (temp) : (-temp);
	
	if( abs(r_target) < abs(r_current) ) {
		switch( skt->scroll ) {
			case SCROLL_22:
				temp = mr_acc;
				break;
			default:
				temp = mr_acc + abs(r_current) * 20 / 100;
				if( temp < 300 ) {
					temp = 300;
				}
				break;
		}
	}
	else {
		temp = mr_acc;
	}
	temp /= 100;
	if( temp < 1 ) {
		temp = 1;
	}
	skt->pwmr_Step = (r_target > r_current) ? (temp) : (-temp);
}

#define TARGET_PWM_OFFSET				6
#define DELTA_PWM_OFFSET				200
#define X0_OFFSET				        200
#define Y0_OFFSET				        200
#define PWM_CALC_FACTOR					6000

static int16_t hz_timer = 50;		//250ms
void Motor_PwmCalc( __motor_t* motor, handle_t* handle )
{
	int32_t pwml  = 0;
	int32_t pwmr  = 0;
	int32_t max   = 0;
	int32_t back_max  = BACK_DUTY_MAX;
	int32_t dlt_curr  = 0;
	int32_t dlt_tgt   = 0;
	int32_t dlt_left  = 0;
	int32_t dlt_right = 0;
	int32_t delta = 0;
	sysmgmt_t* mgmt = &SysMgmt;
	
	if( (abs(handle->rawCountX - handle->hallposX0) < COUNT_OFFSET_X) && \
		(abs(handle->rawCountY - handle->hallposY0) < COUNT_OFFSET_Y) ) {
		if( hz_timer ) {
			hz_timer--;
		}
		motor->pwml_Target = 0;
		motor->pwmr_Target = 0;
		motor->left_middle_Target = 0;
		motor->right_middle_Target = 0;
		pwml  = 0;
		pwmr  = 0;
		max   = 0;
		motor->Status_bit.special_handle   = false;
		motor->Status_bit.L_reach_m_target = false;
		motor->Status_bit.R_reach_m_target = false;
		mgmt->Status.handle_zero = true;
		SysMgmt_ErrorClr( mgmt, ERROR_OVER_CURRENT );
		return;
	}
	
//	if( SysMgmt_ErrorChk(mgmt, ERROR_OVER_CURRENT) == true ) {
//		return;
//	}
	
	hz_timer = 50;
	mgmt->Status.handle_zero = false;
	max = motor->maxpwm[motor->speedlevel];
	back_max = -(max / 2);
	if( handle->rawCountY > (handle->hallposY0 + Y0_OFFSET) ) {
		pwml  = ((handle->rawCountY - handle->hallposY0) * 35) - ((handle->rawCountX - handle->hallposX0) * 10);
		pwml /= 10;
		pwml *= max;
		pwml /= PWM_CALC_FACTOR;
		
		if( pwml > max ) {
			pwml = max;
		}
		if( pwml < back_max ) {
			pwml = back_max;
		}
		pwml *= motor->speed_k;
		pwml /= 100;
		motor->pwml_Target = pwml;
		
		pwmr  = ((handle->rawCountY - handle->hallposY0) * 35) + ((handle->rawCountX - handle->hallposX0) * 10);
		pwmr /= 10;
		pwmr *= max;
		pwmr /= PWM_CALC_FACTOR;
		
		if( pwmr > max ) {
			pwmr = max;
		}
		if( pwmr < back_max ) {
			pwmr = back_max;
		}
		pwmr *= motor->speed_k;
		pwmr /= 100;
		motor->pwmr_Target = pwmr;
	}
	else {
		pwml  = ((handle->rawCountY - handle->hallposY0) * 20) - ((handle->rawCountX - handle->hallposX0) * 10);
		pwml /= 10;
		pwml *= max;
		pwml /= PWM_CALC_FACTOR;
		
		if( pwml > max ) {
			pwml = max;
		}
		if( pwml < back_max ) {
			pwml = back_max;
		}
		pwml *= motor->speed_k;
		pwml /= 100;
		motor->pwml_Target = pwml;
		
		pwmr  = ((handle->rawCountY - handle->hallposY0) * 20) + ((handle->rawCountX - handle->hallposX0) * 10);
		pwmr /= 10;
		pwmr *= max;
		pwmr /= PWM_CALC_FACTOR;
		
		if( pwmr > max ) {
			pwmr = max;
		}
		if( pwmr < back_max ) {
			pwmr = back_max;
		}
		pwmr *= motor->speed_k;
		pwmr /= 100;
		motor->pwmr_Target = pwmr;
	}
	motor->pwmr_Target = (motor->pwmr_Target * motor->speedratio) / 100;
	
	dlt_curr  = motor->pwmr_Current - motor->pwml_Current;	//a
	dlt_tgt   = motor->pwmr_Target  - motor->pwml_Target;	//b
	dlt_left  = motor->pwml_Target  - motor->pwml_Current;	//c
	dlt_right = motor->pwmr_Target  - motor->pwmr_Current;	//d
	delta = abs(dlt_left - dlt_right) / 2;				//|c-d|/2=e
	if( delta >= DELTA_PWM_OFFSET ) {	//200
		if( (((dlt_curr > 0) && (dlt_curr > dlt_tgt)) || ((dlt_curr < 0) && (dlt_curr < dlt_tgt))) && \
			(((dlt_left > 0) && (dlt_right > 0)) || ((dlt_left < 0) && (dlt_right < 0))) ) {//R
			if( abs(dlt_right) >= abs(dlt_left) ) {
				if( dlt_left > 0 ) {	//c>0
					motor->left_middle_Target = motor->pwml_Current - delta;	//o1-e
				}
				else {
					motor->left_middle_Target = motor->pwml_Current + delta;	//o1+e
				}
				motor->right_middle_Target = motor->pwmr_Target;
				motor->Status_bit.L_reach_m_target = false;
				motor->Status_bit.R_reach_m_target = true;
			}
			else {
				if( dlt_right > 0 ) {	//d>0
					motor->right_middle_Target = motor->pwmr_Current - delta;
				}
				else {
					motor->right_middle_Target = motor->pwmr_Current + delta;
				}
				motor->left_middle_Target = motor->pwml_Target;
				motor->Status_bit.L_reach_m_target = true;
				motor->Status_bit.R_reach_m_target = false;
			}
			motor->Status_bit.special_handle = true;
		}
		else {
			motor->left_middle_Target  = motor->pwml_Target;
			motor->right_middle_Target = motor->pwmr_Target;
			motor->Status_bit.special_handle = false;
		}
	}
	else {
		motor->left_middle_Target  = motor->pwml_Target;
		motor->right_middle_Target = motor->pwmr_Target;
		motor->Status_bit.special_handle = false;
	}
	
	if( motor->Status_bit.run_dir == true ) {
		motor->Status_bit.run_dir = false;
		pwml = (motor->pwml_Target + motor->pwmr_Target) / 2;
		motor->pwml_Target = pwml;
		motor->pwmr_Target = pwml;
	}
}

void Motor_PwmOutProc( __motor_t* skt )
{
	if( (Handle_ZeroChkRes() == false) || 										 \
		(SysMgmt.Status.enter_handle_eng == true) || 							 \
		(SysMgmt_ErrorChk(&SysMgmt, (ERROR_L_BREAK | ERROR_R_BREAK) == true)) || \
		(skt->Status_bit.motor_out_en == false) ) {
		HAL_TIM_PWM_Reset();
	  	return;
	}
	
	Motor_CalcStep( skt );
	//motor A
	if( abs(skt->pwml_Target - skt->pwml_Current) > TARGET_PWM_OFFSET ) {
		skt->pwml_Current += skt->pwml_Step;
		if( abs(skt->pwml_Target - skt->pwml_Current) < skt->pwml_Step ) {
			skt->pwml_Current = skt->pwml_Target;
			skt->Status_bit.L_reach_m_target = false;
			skt->Status_bit.special_handle = false;
		}
	}
	else {
		if( skt->pwml_Current != skt->pwml_Target ) {
			skt->pwml_Current = skt->pwml_Target;
			skt->Status_bit.L_reach_m_target = false;
			skt->Status_bit.special_handle = false;
		}
	}
	if( abs(skt->left_middle_Target - skt->pwml_Current) < skt->pwml_Step ) {
		skt->Status_bit.L_reach_m_target = true;
	}
	//motor B
	if( abs(skt->pwmr_Target - skt->pwmr_Current) > TARGET_PWM_OFFSET ) {
		skt->pwmr_Current += skt->pwmr_Step;
		if( abs(skt->pwmr_Target - skt->pwmr_Current) < skt->pwmr_Step ) {
			skt->pwmr_Current = skt->pwmr_Target;
			skt->Status_bit.R_reach_m_target = false;
			skt->Status_bit.special_handle = false;
		}
	}
	else {
		if( skt->pwmr_Current != skt->pwmr_Target ) {
			skt->pwmr_Current = skt->pwmr_Target;
			skt->Status_bit.R_reach_m_target = false;
			skt->Status_bit.special_handle = false;
		}
	}
	if( abs(skt->right_middle_Target - skt->pwmr_Current) < skt->pwmr_Step ) {
		skt->Status_bit.R_reach_m_target = true;
	}
	Motor_PwmOutControl( skt );
}

static uint16_t motorActDlyTimer = 10;
static uint16_t motorStpDlyTimer = 0;
void Motor_PwmOutControl( __motor_t* skt )
{
	if( SysMgmt.Status.e_breaker_en == true ) {
		if( (abs(skt->pwml_Current) > 100) || (abs(skt->pwmr_Current) > 100) ) {
			if( skt->Status_bit.motor_pwm_out == false ) {
				if( skt->Status_bit.motor_action_dly == false ) {
					skt->Status_bit.motor_action_dly = true;
					HAL_GPIO_WritePin(GPIOB, GPIO_PIN_7, GPIO_PIN_RESET);	//unbreak
					motorActDlyTimer = 0;			//50ms
				}
			}
		}
		else {
			if( ((abs(skt->pwml_Target) < 50) && (abs(skt->pwml_Current) < 50)) && \
				((abs(skt->pwmr_Target) < 50) && (abs(skt->pwmr_Current) < 50)) ) {
				if( skt->Status_bit.motor_pwm_out == true ) {
					if( skt->Status_bit.motor_stop_dly == false ) {
						skt->Status_bit.motor_stop_dly = true;
						motorStpDlyTimer = 0;		//100ms
					}
				}
			}
		}
	}
	else {
		skt->Status_bit.motor_action_dly = false;
		skt->Status_bit.motor_stop_dly   = false;
		skt->Status_bit.motor_pwm_out    = true;
	}
	if( skt->Status_bit.motor_pwm_out == true ) {
		HAL_TIM_PWM_Load( MOTOR_L, skt->pwml_Current );
		HAL_TIM_PWM_Load( MOTOR_R, skt->pwmr_Current );
		if( (abs(skt->pwml_Current) > 800) || (abs(skt->pwmr_Current) > 800) ) {
			//SysMgmt_ExceptionChk( &SysMgmt, SYS_EXCEP_OVER_CURRENT );
		}
	}
}

void Motor_PwmOutDlyTimer( __motor_t* skt )
{
	if( motorActDlyTimer ) {
		motorActDlyTimer--;
	}
	else {
		if( skt->Status_bit.motor_action_dly == true ) {
			skt->Status_bit.motor_action_dly = false;
			skt->Status_bit.motor_pwm_out = true;
		}
	}
	
	if( motorStpDlyTimer ) {
		motorStpDlyTimer--;
	}
	else {
		if( skt->Status_bit.motor_stop_dly == true ) {
			skt->Status_bit.motor_stop_dly = false;
			skt->Status_bit.motor_pwm_out  = false;
			HAL_GPIO_WritePin(GPIOB, GPIO_PIN_7, GPIO_PIN_SET);		//break
		}
	}
}

//for rpm calc
RPM st_Rpm;
void Rpm_Init( void )
{
	//IO_RPM_IN;
	memset( (uint8_t*)&st_Rpm, 0x00, sizeof(RPM) );
	st_Rpm.control.b_stop = true;
	st_Rpm.scroll = Motor.scroll;
}

void Rpm_CntOvr( void )
{
	st_Rpm.ovrCnt++;
	if( st_Rpm.ovrCnt >= 23 ) {
		st_Rpm.ovrCnt = 0;
		st_Rpm.control.b_first = false;
		st_Rpm.control.b_stop  = true;
		st_Rpm.control.b_ok    = true;
	}
}

void Rpm_CntLmt( uint16_t u16_hispd, uint16_t u16_lospd )
{
	uint32_t u32_tmp = 0;
	
	u32_tmp = u16_hispd;
	u32_tmp *= 1000000;
	u32_tmp /= st_Rpm.scroll;
	u32_tmp = 2250000000 / u32_tmp;
	st_Rpm.minCnt = u32_tmp;
	
	u32_tmp = u16_lospd;
	u32_tmp *= 1000000;
	u32_tmp /= st_Rpm.scroll;
	u32_tmp = 2250000000 / u32_tmp;
	st_Rpm.maxCnt = u32_tmp;
}

void Rpm_CalCnt( void )
{
	static uint32_t u32_pre = 0;
	uint32_t u32_tmp = 0;
	
	u32_tmp = (st_Rpm.ovrCnt << 16);
	u32_tmp = u32_tmp + 0;//trdgrb0;
	u32_tmp = u32_tmp - u32_pre;
	if( u32_tmp > st_Rpm.minCnt ) {
		u32_pre = 0;//trdgrb0;
		st_Rpm.rpmCnt = u32_tmp;
		if( st_Rpm.control.b_first ) {
			st_Rpm.control.b_ok = true;
		}
		st_Rpm.control.b_first = true;
		st_Rpm.control.b_stop  = false;
	}
	st_Rpm.ovrCnt = 0;
}

static uint16_t spdBuff[5] = { 0, 0, 0, 0, 0 };
void Rpm_Dcd( void )
{
	uint32_t u32_tmp1;
	uint16_t u16_tmp2;
	uint16_t u16_tmp3;
	
	if( st_Rpm.control.b_ok == false ) {
		return;
	}
	
	st_Rpm.control.b_ok = false;
	if( st_Rpm.control.b_stop == true ) {
		st_Rpm.rpmSpd = 0;
		return;
	}
	u32_tmp1 = st_Rpm.rpmCnt;
	u32_tmp1 = 2250000000/u32_tmp1;
	u32_tmp1 *= st_Rpm.scroll;
	if( st_Rpm.control.b_spd_mode == C_KM ){
		u32_tmp1 /= 100000;
	}
	else {
		u32_tmp1 /= 160900;
	}
	spdBuff[4] = spdBuff[3];
	spdBuff[3] = spdBuff[2];
	spdBuff[2] = spdBuff[1];
	spdBuff[1] = spdBuff[0];
	spdBuff[0] = (uint16_t)u32_tmp1;
	
	switch( spdBuff[0]/200 ) {
		case 0: 
			st_Rpm.rpmSpd = spdBuff[0];
			break;
		case 1:
			st_Rpm.rpmSpd = (spdBuff[0] + spdBuff[1])/2;
			break;
		case 2:
			st_Rpm.rpmSpd = (spdBuff[0] + spdBuff[1] + spdBuff[2])/3;
			break;
		case 3:
			st_Rpm.rpmSpd = (spdBuff[0] + spdBuff[1] + spdBuff[2] + spdBuff[3])/4;
			break;
		default:
			st_Rpm.rpmSpd = (spdBuff[0] + spdBuff[1] + spdBuff[2] + spdBuff[3] + spdBuff[4])/5;
			break;
	}
	if( !st_Rpm.control.b_500ms ) {
		return;
	}
	
	st_Rpm.control.b_500ms = 0;
	st_Rpm.speed[2] = st_Rpm.speed[1];
	st_Rpm.speed[1] = st_Rpm.speed[0];
	st_Rpm.speed[0] = st_Rpm.rpmSpd;
	if( st_Rpm.speed[2] == 0 ) {
		return;
	}
	
	if( st_Rpm.speed[2] >= st_Rpm.speed[1] ) {
		u16_tmp2 = st_Rpm.speed[2] - st_Rpm.speed[1];
	}
	else {
		u16_tmp2 = st_Rpm.speed[1] - st_Rpm.speed[2];
	}
	
	if (st_Rpm.speed[1] >= st_Rpm.speed[0] ) {
		u16_tmp3 = st_Rpm.speed[1] - st_Rpm.speed[0];
	}
	else {
		u16_tmp3 = st_Rpm.speed[0] - st_Rpm.speed[1];
	}
	
	if( (u16_tmp2 < RPM_OFFSET) && (u16_tmp3 < RPM_OFFSET) ) {
		st_Rpm.control.b_stab = 1;
	}
	else {
		st_Rpm.control.b_stab = 0;
	}
}

void Rpm_ClrBuf( void )
{
	st_Rpm.control.b_stab = 0;
	st_Rpm.speed[2] = 0;
	st_Rpm.speed[1] = 0;
	st_Rpm.speed[0] = 0;
}

void Motor_AutoAdjust( void )
{
	
}

