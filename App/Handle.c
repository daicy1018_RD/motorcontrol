/*******************************************************************************

   (C) Copyright 2015/10/25, Chaoyi Dai. 	 All rights reserved

----------------- File Information ---------------------------------------------
       File Name: handle.c
          Author: Chaoyi Dai
Last Modify Date: 2015/10/25
     Discription:

---------------- Important Notes -----------------------------------------------


----------------- Rivision History ---------------------------------------------
         Version:
          Author: Chaoyi Dai
Last Modify Date: 2015/10/25
     Discription: Initial version

*******************************************************************************/

#include "includes.h"
#include "adc.h"
#include "stm32f1xx_hal.h"
#include "tim.h"


#define HANDLE_POSI_FRONT_DEF			3846
#define HANDLE_POSI_BACK_DEF			250
#define HANDLE_POSI_LEFT_DEF			3846
#define HANDLE_POSI_RIGHT_DEF			250
#define HANDLE_X_RANGE					((HANDLE_POSI_LEFT_DEF - HANDLE_POSI_RIGHT_DEF) / 2)
#define HANDLE_Y_RANGE					((HANDLE_POSI_FRONT_DEF - HANDLE_POSI_BACK_DEF) / 2)

#define FIFO_HANDLE_X_BUFF_NBR			8
#define FIFO_HANDLE_Y_BUFF_NBR			8
__no_init __fifo_t fifo_handle_x;
__no_init int32_t fifo_handle_x_buff[FIFO_HANDLE_X_BUFF_NBR];
__no_init __fifo_t fifo_handle_y;
__no_init int32_t fifo_handle_y_buff[FIFO_HANDLE_Y_BUFF_NBR];

__no_init handle_t Handle;


void Handle_Init( void )
{
	handle_t* skt = &Handle;
	
	Fifo_Init( &fifo_handle_x, fifo_handle_x_buff, FIFO_HANDLE_X_BUFF_NBR );
	Fifo_Init( &fifo_handle_y, fifo_handle_y_buff, FIFO_HANDLE_Y_BUFF_NBR );
	
    memset( skt, 0x00, sizeof(handle_t) );
}

#define UPDATA_OFFSET           20
#define UPDATA_OFFSET_MAX       50
#define UPDATA_OFFSET_MIN       20
#define UPDATA_OFFSET_DIR       50

bool Handle_ConnChk( handle_t* skt )
{
	if( (skt->rawCountX < UPDATA_OFFSET) && (skt->rawCountY < UPDATA_OFFSET) ) {
		return( true );
	}
	
	return( false );
}

#define  DC_ORG_Y0    		2048
#define  DC_ORG_X0    		2048

void Handle_PosiGet( handle_t* skt )
{
	bool     update = false;
	int32_t  temp;
	int32_t  hx_count;
	int32_t  hy_count;
	int32_t  offset = 0;
	int32_t  offsetlimitx;
	int32_t  offsetlimity;
	int32_t  x_range;
	int32_t  y_range;
	__motor_t* motor = &Motor;
	
	if( (SysMgmt.Status.para_got == false) || \
	    (SysMgmt.Status.power_on == false) ) {
		return;
	}
	
	hx_count = Fifo_Proc( &fifo_handle_x, Get_adc(ADC_CHANNEL_8) );
	hy_count = Fifo_Proc( &fifo_handle_y, Get_adc(ADC_CHANNEL_9) );
	if( skt->Status_bit.zero_ok == false ) {
		Handle_ZeroChk( skt, hx_count, hy_count );
		return;
	}
	
	temp = hx_count;
	if( skt->p_x_left > 0 ) {
		if( temp >= skt->adc_x0 ) {		//left
			hx_count = (((temp - skt->adc_x0) * skt->p_x_left)/100 + DC_ORG_X0);
		}
		else {							//right
			hx_count = (((temp - skt->adc_x0) * skt->p_x_right)/100 + DC_ORG_X0);
		}
	}
	else {
		if( temp >= skt->adc_x0 ) {		//right
			hx_count = (((temp - skt->adc_x0) * skt->p_x_right)/100 + DC_ORG_X0);
		}
		else {							//left
			hx_count = (((temp - skt->adc_x0) * skt->p_x_left)/100 + DC_ORG_X0);
		}
	}
	
	offset = abs( skt->rawCountX - hx_count );
	if( offset > UPDATA_OFFSET_MAX ) {
		update = true;
	}
	else {
		x_range = HANDLE_X_RANGE;
		offsetlimitx  = abs(hx_count - skt->hallposX0) * (UPDATA_OFFSET_MAX - UPDATA_OFFSET_MIN) / x_range;
		offsetlimitx += UPDATA_OFFSET_MIN;
		if( offset > offsetlimitx ) {
			update = true;
		}
		else {
			if( (abs(hx_count - skt->hallposX0) < offsetlimitx) && (abs(skt->rawCountX - skt->adc_x0) > offsetlimitx) ) {
				update = true;
			}
		}
	}
	if( abs(hx_count - skt->hallposX0) < UPDATA_OFFSET_DIR ) {
		motor->Status_bit.run_dir = true;
	}
	if( update == true ) {
		update = false;
		skt->rawCountX = hx_count;
		skt->Status_bit.update = true;
	}
	
	temp = hy_count;
	if( skt->p_y_front > 0 ) {
		if( temp >= skt->adc_y0 ) {		//front
			hy_count = (((temp - skt->adc_y0) * skt->p_y_front)/100 + DC_ORG_Y0);
		}
		else {							//back
			hy_count = (((temp - skt->adc_y0) * skt->p_y_back)/100 + DC_ORG_Y0);
		}
	}
	else {
		if( temp >= skt->adc_y0 ) { 	//back
			hy_count = (((temp - skt->adc_y0) * skt->p_y_back)/100 + DC_ORG_Y0);
		}
		else {							//front
			hy_count = (((temp - skt->adc_y0) * skt->p_y_front)/100 + DC_ORG_Y0);
		}
	}
	
	offset = abs( skt->rawCountY - hy_count );
	if( offset > UPDATA_OFFSET_MAX ) {
		update = true;
	}
	else {
		y_range = HANDLE_Y_RANGE;
		offsetlimity  = abs(hy_count - skt->hallposY0) * (UPDATA_OFFSET_MAX - UPDATA_OFFSET_MIN) / y_range;
		offsetlimity += UPDATA_OFFSET_MIN;
		if( offset > offsetlimity ) {
			update = true;
		}
	}
	if( update == true ) {
		update = false;
		skt->rawCountY = hy_count;
		skt->Status_bit.update = true;
	}
}

#define ADJUST_TIME_2000MS			(200)		//400 * 5ms = 2000ms
#define ADJUST_OFFSET				(500)

void Handle_ZeroChk( handle_t* skt, uint32_t hx_adc, uint32_t hy_adc )
{
    if( (hx_adc > (skt->adc_x0 + ADJUST_OFFSET)) || \
		(hx_adc < (skt->adc_x0 - ADJUST_OFFSET)) || \
		(hy_adc > (skt->adc_y0 + ADJUST_OFFSET)) || \
		(hy_adc < (skt->adc_y0 - ADJUST_OFFSET)) ) {
		skt->adjust_Time = 0;
	}
    skt->adjust_adcx[2] = skt->adjust_adcx[1];
	skt->adjust_adcx[1] = skt->adjust_adcx[0];
	skt->adjust_adcx[0] = hx_adc;
	
    skt->adjust_adcy[2] = skt->adjust_adcy[1];
	skt->adjust_adcy[1] = skt->adjust_adcy[0];
	skt->adjust_adcy[0] = hy_adc;
	
    if( (((skt->adjust_adcx[0] + ADJUST_OFFSET) > skt->adc_x0) && (skt->adjust_adcx[0] < (skt->adc_x0 + ADJUST_OFFSET))) && \
	    (((skt->adjust_adcx[1] + ADJUST_OFFSET) > skt->adc_x0) && (skt->adjust_adcx[1] < (skt->adc_x0 + ADJUST_OFFSET))) && \
	    (((skt->adjust_adcx[2] + ADJUST_OFFSET) > skt->adc_x0) && (skt->adjust_adcx[2] < (skt->adc_x0 + ADJUST_OFFSET))) && \
		(((skt->adjust_adcy[0] + ADJUST_OFFSET) > skt->adc_y0) && (skt->adjust_adcy[0] < (skt->adc_y0 + ADJUST_OFFSET))) && \
	  	(((skt->adjust_adcy[1] + ADJUST_OFFSET) > skt->adc_y0) && (skt->adjust_adcy[1] < (skt->adc_y0 + ADJUST_OFFSET))) && \
	  	(((skt->adjust_adcy[2] + ADJUST_OFFSET) > skt->adc_y0) && (skt->adjust_adcy[2] < (skt->adc_y0 + ADJUST_OFFSET))) ) {
		skt->adjust_Time++;
		if( skt->adjust_Time >= ADJUST_TIME_2000MS ) {
			skt->Status_bit.zero_ok = true;
			skt->hallposX0 = DC_ORG_X0;
			skt->hallposY0 = DC_ORG_Y0;
		}
	}
	else {
		skt->adjust_Time = 0;
	}
	//SysMgmt_ExceptionChk( skt, SYS_EXCEP_HANDLE_CONN );
}

bool Handle_ZeroChkRes( void )
{
	return( Handle.Status_bit.zero_ok );
}

