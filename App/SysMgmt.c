/*******************************************************************************

   (C) Copyright 2015/10/25, Chaoyi Dai. 	 All rights reserved

----------------- File Information ---------------------------------------------
       File Name: SysMgmt.c
          Author: Chaoyi Dai
Last Modify Date: 2015/10/25
     Discription: 

---------------- Important Notes -----------------------------------------------


----------------- Rivision History ---------------------------------------------
         Version: 
          Author: Chaoyi Dai
Last Modify Date: 2015/10/25
     Discription: Initial version

*******************************************************************************/

#include "includes.h"
#include "adc.h"
#include "stm32f1xx_hal.h"
#include "tim.h"

#define FIFO_BATT_BUFF_NBR					8
#define FIFO_OVR_CURR_BUFF_NBR				8
#define FIFO_E_BREAK_BUFF_NBR				8
#define FIFO_MOTOR_CONN_1_BUFF_NBR			8
#define FIFO_MOTOR_CONN_2_BUFF_NBR			8

__no_init sysmgmt_t SysMgmt;
__no_init __fifo_t fifo_batt;
__no_init int32_t fifo_batt_buff[FIFO_BATT_BUFF_NBR];

__no_init __fifo_t fifo_l1_ovrCurr;
__no_init int32_t fifo_l1_ovrCurr_buff[FIFO_OVR_CURR_BUFF_NBR];
__no_init __fifo_t fifo_l2_ovrCurr;
__no_init int32_t fifo_l2_ovrCurr_buff[FIFO_OVR_CURR_BUFF_NBR];
__no_init __fifo_t fifo_r1_ovrCurr;
__no_init int32_t fifo_r1_ovrCurr_buff[FIFO_OVR_CURR_BUFF_NBR];
__no_init __fifo_t fifo_r2_ovrCurr;
__no_init int32_t fifo_r2_ovrCurr_buff[FIFO_OVR_CURR_BUFF_NBR];

__no_init __fifo_t fifo_ebreak;
__no_init int32_t fifo_ebreak_buff[FIFO_E_BREAK_BUFF_NBR];
__no_init __fifo_t fifo_motor_conn_1;
__no_init int32_t fifo_motor_conn_1_buff[FIFO_MOTOR_CONN_1_BUFF_NBR];
__no_init __fifo_t fifo_motor_conn_2;
__no_init int32_t fifo_motor_conn_2_buff[FIFO_MOTOR_CONN_2_BUFF_NBR];


void SysMgmt_Init( void )
{
    memset( &SysMgmt, 0x0, sizeof(sysmgmt_t) );
	SysMgmt.CommTimeout = 5;
	Fifo_Init( &fifo_batt, fifo_batt_buff, FIFO_BATT_BUFF_NBR );
	Fifo_Init( &fifo_l1_ovrCurr, fifo_l1_ovrCurr_buff, FIFO_OVR_CURR_BUFF_NBR );
	Fifo_Init( &fifo_l2_ovrCurr, fifo_l2_ovrCurr_buff, FIFO_OVR_CURR_BUFF_NBR );
	Fifo_Init( &fifo_r1_ovrCurr, fifo_r1_ovrCurr_buff, FIFO_OVR_CURR_BUFF_NBR );
	Fifo_Init( &fifo_r2_ovrCurr, fifo_r2_ovrCurr_buff, FIFO_OVR_CURR_BUFF_NBR );
	
	Fifo_Init( &fifo_ebreak, fifo_ebreak_buff, FIFO_E_BREAK_BUFF_NBR );
	Fifo_Init( &fifo_motor_conn_1, fifo_motor_conn_1_buff, FIFO_MOTOR_CONN_1_BUFF_NBR );
	Fifo_Init( &fifo_motor_conn_2, fifo_motor_conn_2_buff, FIFO_MOTOR_CONN_2_BUFF_NBR );
}

bool SysMgmt_PowerChk( void )
{
    if( HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_13) == GPIO_PIN_RESET ) {
        return( true );
    }
    
    return( false );
}

void SysMgmt_DevicesInit( void )
{
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_15, GPIO_PIN_SET);	//power 5V
	while( 1 ) {
		//Comm_WithDashboardTask();
		if( (SysMgmt.Status.power_on == true) && \
			(SysMgmt.Status.para_got == true) ) {
			break;
		}
	}
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_8, GPIO_PIN_SET);		//power 15V
	HAL_Delay( 200 );    	//400ms
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_12, GPIO_PIN_SET);	//relay enable
	HAL_Delay( 200 );
	
	SysMgmt_ExceptionChk( &SysMgmt, SYS_EXCEP_MOTOR_CONN );
	//HAL_GPIO_WritePin(GPIOB, GPIO_PIN_7, GPIO_PIN_SET);		//break
	HAL_Delay( 10 );
	SysMgmt_ExceptionChk( &SysMgmt, SYS_EXCEP_BREAK );
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_12, GPIO_PIN_RESET);	//exp buzzer
	SysMgmt_ExceptionChk( &SysMgmt, SYS_EXCEP_BATT_CAPACITY );
}

void SysMgmt_ShutdownCtrl( void )
{
	__motor_t* motor  = &Motor;
	handle_t*  handle = &Handle;
	
	motor->pwmr_Target = 0;
	motor->pwml_Target = 0;
	if( (abs(motor->pwml_Current) > 50) || (abs(motor->pwmr_Current) > 50) ) {
		handle->rawCountX = handle->hallposX0;
		handle->rawCountY = handle->hallposY0;
		while( true ) {
			if( (motor->pwml_Current == 0) && (motor->pwmr_Current == 0) ) {
				break;
			}
		}
	}
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_12, GPIO_PIN_RESET);	//relay disable
	HAL_Delay( 50 );
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_8, GPIO_PIN_RESET);	//power 15V disable
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_15, GPIO_PIN_RESET);	//power 5V
	//while( true );	//shutdown. . . 
}

void SysMgmt_Power15V( bool enable )
{
    if( enable == true ) {
        HAL_GPIO_WritePin(GPIOB, GPIO_PIN_8, GPIO_PIN_RESET);
        return;
    }
	
    HAL_GPIO_WritePin(GPIOB, GPIO_PIN_8, GPIO_PIN_SET);
}

void SysMgmt_ErrorSet( sysmgmt_t* mgmt, uint32_t err )
{
	if( !(mgmt->sysExcep.ErrCode & err) ) {
		mgmt->sysExcep.ErrCode |= err;
	}
}

void SysMgmt_ErrorClr( sysmgmt_t* mgmt, uint32_t err )
{
	if( mgmt->sysExcep.ErrCode & err ) {
		mgmt->sysExcep.ErrCode &= ~err;
	}
}

bool SysMgmt_ErrorChk( sysmgmt_t* mgmt, uint32_t err )
{
	return( (mgmt->sysExcep.ErrCode & err) ? (true) : (false) );
}

#define BATT_BASE_COUNTS			3230	//(15/115)*20V/3.3*4095 = 3237
#define BATT_SPAN_COUNTS			4050	//(15/115)*25V/3.3*4095 = 4046
#define BATT_RANGE_COUNT			(BATT_SPAN_COUNTS - BATT_BASE_COUNTS)
#define MOTOR_CONN_ADC				2866	//3.3 * 0.7 * 4095
#define MOTOR_SHUT_CURRENT			3100	//((0.01 * 25A) * 10 / 3.3) * 4095
#define MOTOR_OVER_CURRENT			2400	//((0.01 * 20A) * 10 / 3.3) * 4095 = 2480
#define MOTOR_SAFE_CURRENT			1000	//((0.01 * 10A) * 10 / 3.3) * 4095 = 1240
#define MOTOR_AVG_CURRENT			620		//((0.01 * 5A) * 10 / 3.3) * 4095
#define CURRENT_OVER_TIME			1000	//5s

#define ADC_E_BREAK_L				900		//((5.6 /155.6) * 20 / 3.3) * 4095
#define ADC_E_BREAK_R				1300	//((5.6 /105.6) * 20 / 3.3) * 4095
#define ADC_E_BREAK_LR				2100	//((5.6 /65.6) * 20 / 3.3) * 4095

#define MOTOR_CONN_CHK_PWM			2400


void SysMgmt_ExceptionChk( sysmgmt_t* mgmt, uint8_t type )
{
	uint8_t  flag = 0x0;
	uint8_t  samples = 0;
	int32_t  count1 = 0;
	int32_t  count2 = 0;
	int32_t  count3 = 0;
	int32_t  count4 = 0;
	__motor_t* motor = &Motor;
	
	switch( type ) {
		case SYS_EXCEP_MOTOR_CONN:
			if( mgmt->Status.motor_conn_chked == true ) {
				break;
			}
			//left motor connection check
			Fifo_ClrBuff( &fifo_motor_conn_1 );
			Fifo_ClrBuff( &fifo_motor_conn_2 );
			if( motor->Status_bit.l_motor_front == true ) {
				HAL_TIM_PWM_Out_En( PWM_CH_ML_1, true );
				HAL_TIM_PWM_Out_En( PWM_CH_ML_2, false );
				HAL_Delay( 2 );
				HAL_TIM_PWM_Output( PWM_CH_ML_1, MOTOR_CONN_CHK_PWM );
				HAL_TIM_PWM_Output( PWM_CH_ML_2, 0 );
			}
			else {
				HAL_TIM_PWM_Out_En( PWM_CH_ML_2, true );
				HAL_TIM_PWM_Out_En( PWM_CH_ML_1, false );
				HAL_Delay( 2 );
				HAL_TIM_PWM_Output( PWM_CH_ML_2, MOTOR_CONN_CHK_PWM );
				HAL_TIM_PWM_Output( PWM_CH_ML_1, 0 );
			}
			HAL_Delay( 2 );		//2ms
			samples = 8;
			while( samples-- ) {
				count1 = Fifo_Proc( &fifo_motor_conn_1, Get_adc(ADC_CHANNEL_6) );
				count2 = Fifo_Proc( &fifo_motor_conn_2, Get_adc(ADC_CHANNEL_4) );
			}
			if( motor->Status_bit.l_motor_front == true ) {
				if( count2 > MOTOR_CONN_ADC ) {
					flag |= 0x1;
				}
			}
			else {
				if( count1 > MOTOR_CONN_ADC ) {
					flag |= 0x1;
				}
			}
			HAL_TIM_PWM_Out_En( PWM_CH_ML_1, true );
			HAL_TIM_PWM_Out_En( PWM_CH_ML_2, true );
			HAL_TIM_PWM_Output( PWM_CH_ML_1, 0 );
			HAL_TIM_PWM_Output( PWM_CH_ML_2, 0 );
			//right motor connection check
			
			Fifo_ClrBuff( &fifo_motor_conn_1 );
			Fifo_ClrBuff( &fifo_motor_conn_2 );
			if( motor->Status_bit.r_motor_front == true ) {
				HAL_TIM_PWM_Out_En( PWM_CH_MR_1, true );
				HAL_TIM_PWM_Out_En( PWM_CH_MR_2, false );
				HAL_Delay( 2 );
				HAL_TIM_PWM_Output( PWM_CH_MR_1, MOTOR_CONN_CHK_PWM );
				HAL_TIM_PWM_Output( PWM_CH_MR_2, 0 );
			}
			else {
				HAL_TIM_PWM_Out_En( PWM_CH_MR_2, true );
				HAL_TIM_PWM_Out_En( PWM_CH_MR_1, false );
				HAL_Delay( 2 );
				HAL_TIM_PWM_Output( PWM_CH_MR_2, MOTOR_CONN_CHK_PWM );
				HAL_TIM_PWM_Output( PWM_CH_MR_1, 0 );
			}
			HAL_Delay( 2 );		//2ms
			samples = 8;
			while( samples-- ) {
				count3 = Fifo_Proc( &fifo_motor_conn_1, Get_adc(ADC_CHANNEL_7) );
				count4 = Fifo_Proc( &fifo_motor_conn_2, Get_adc(ADC_CHANNEL_5) );
			}
			if( motor->Status_bit.r_motor_front == true ) {
				if( count4 > MOTOR_CONN_ADC ) {
					flag |= 0x2;
				}
			}
			else {
				if( count3 > MOTOR_CONN_ADC ) {
					flag |= 0x2;
				}
			}
			HAL_TIM_PWM_Out_En( PWM_CH_MR_1, true );
			HAL_TIM_PWM_Out_En( PWM_CH_MR_2, true );
			HAL_TIM_PWM_Output( PWM_CH_MR_1, 0 );
			HAL_TIM_PWM_Output( PWM_CH_MR_2, 0 );
			if( flag != 0x3 ) {
				SysMgmt_ErrorSet( mgmt, ERROR_MOTOR_CONN );
			}
			else {
				SysMgmt_ErrorClr( mgmt, ERROR_MOTOR_CONN );
			}
			mgmt->Status.motor_conn_chked = true;
			break;
		case SYS_EXCEP_BATT_CAPACITY:
			samples = 8;
			while( samples-- ) {
				count1 = Fifo_Proc( &fifo_batt, Get_adc(ADC_CHANNEL_1) );
			}
			count1 = count1 - BATT_BASE_COUNTS;
			if( count1 < 0 ) {
				count1 = 0;
			}
			count1 = count1 * 100 / BATT_RANGE_COUNT;
			if( count1 > 100 ) {
				count1 = 100;
			}
			if( count1 < 0 ) {
				count1 = 0;
			}
			mgmt->PowerCapacity = count1;
			break;
		case SYS_EXCEP_AVG_CURRENT:
		case SYS_EXCEP_OVER_CURRENT:
			if( (motor->pwml_Target != 0) || (motor->pwmr_Target != 0) ) {
				if( motor->pwml_Current > 0 ) {
					samples = 4;
					while( samples-- ) {
						count2 = Fifo_Proc( &fifo_l2_ovrCurr, Get_adc(ADC_CHANNEL_4) );
					}
					if( count2 > MOTOR_SHUT_CURRENT ) {
						flag = 0xff;
					}
					else if( count2 > MOTOR_OVER_CURRENT ) {
						if( mgmt->CurrentOverCnt ) {
							mgmt->CurrentOverCnt--;
						}
						else {
							flag |= 0x1;
						}
					}
					else {
						if( count2 < MOTOR_SAFE_CURRENT ) {
							flag &= ~0x1;
							mgmt->CurrentOverCnt = CURRENT_OVER_TIME;
						}
					}
				}
				else if( motor->pwml_Current < 0 ) {
					samples = 4;
					while( samples-- ) {
						count1 = Fifo_Proc( &fifo_l1_ovrCurr, Get_adc(ADC_CHANNEL_6) );
					}
					if( count1 > MOTOR_SHUT_CURRENT ) {
						flag = 0xff;
					}
					else if( count1 > MOTOR_OVER_CURRENT ) {
						if( mgmt->CurrentOverCnt ) {
							mgmt->CurrentOverCnt--;
						}
						else {
							flag |= 0x1;
						}
					}
					else {
						if( count1 < MOTOR_SAFE_CURRENT ) {
							flag &= ~0x1;
							mgmt->CurrentOverCnt = CURRENT_OVER_TIME;
						}
					}
				}
				
				if( motor->pwmr_Current > 0 ) {			//right motor
					samples = 4;
					while( samples-- ) {
						count4 = Fifo_Proc( &fifo_r2_ovrCurr, Get_adc(ADC_CHANNEL_5) );
					}
					if( count4 > MOTOR_SHUT_CURRENT ) {
						flag = 0xff;
					}
					else if( count4 > MOTOR_OVER_CURRENT ) {
						if( mgmt->CurrentOverCnt ) {
							mgmt->CurrentOverCnt--;
						}
						else {
							flag |= 0x2;
						}
					}
					else {
						if( count4 < MOTOR_SAFE_CURRENT ) {
							flag &= ~0x2;
							mgmt->CurrentOverCnt = CURRENT_OVER_TIME;
						}
					}
				}
				else if( motor->pwmr_Current < 0 ) {	//right motor
					samples = 4;
					while( samples-- ) {
						count3 = Fifo_Proc( &fifo_r1_ovrCurr, Get_adc(ADC_CHANNEL_7) );
					}
					if( count3 > MOTOR_SHUT_CURRENT ) {
						flag = 0xff;
					}
					else if( count3 > MOTOR_OVER_CURRENT ) {
						if( mgmt->CurrentOverCnt ) {
							mgmt->CurrentOverCnt--;
						}
						else {
							flag |= 0x2;
						}
					}
					else {
						if( count3 < MOTOR_SAFE_CURRENT ) {
							flag &= ~0x2;
							mgmt->CurrentOverCnt = CURRENT_OVER_TIME;
						}
					}
				}
				
				if( flag == 0xff ) {
					motor->pwml_Target = 0;
					motor->pwmr_Target = 0;
					SysMgmt_ErrorSet( mgmt, ERROR_OVER_CURRENT );
				}
				else {
					if( flag & 0x3 ) {
						motor->pwml_Target = motor->pwml_Target * 90 / 100;
						motor->pwmr_Target = motor->pwmr_Target * 90 / 100;
						SysMgmt_ErrorSet( mgmt, ERROR_OVER_CURRENT );
					}
				}
			}
			else {
				SysMgmt_ErrorClr( mgmt, ERROR_OVER_CURRENT );
			}
			break;
		case SYS_EXCEP_HANDLE_CONN:
			if( Handle_ConnChk(&Handle) == true ) {
				SysMgmt_ErrorSet( mgmt, ERROR_HANDLE_CONN );
			}
			else {
				SysMgmt_ErrorClr( mgmt, ERROR_HANDLE_CONN );
			}
			break;
		case SYS_EXCEP_BREAK:
			samples = 16;
			while( samples-- ) {
				count1 = Fifo_Proc( &fifo_ebreak, Get_adc(ADC_CHANNEL_0) );
			}
			if( count1 < (ADC_E_BREAK_L - 500) ) {
				HAL_GPIO_WritePin(GPIOB, GPIO_PIN_7, GPIO_PIN_RESET);
				mgmt->Status.e_breaker_en = false;
			}
			else if( (count1 > (ADC_E_BREAK_L + 200)) && (count1 < (ADC_E_BREAK_R - 500)) ) {
				SysMgmt_ErrorClr( mgmt, ERROR_L_BREAK );
				SysMgmt_ErrorSet( mgmt, ERROR_R_BREAK );
			}
			else if( (count1 > (ADC_E_BREAK_R + 200)) && (count1 < (ADC_E_BREAK_LR - 500)) ) {
				SysMgmt_ErrorClr( mgmt, ERROR_R_BREAK );
				SysMgmt_ErrorSet( mgmt, ERROR_L_BREAK );
			}
			else {
				mgmt->Status.e_breaker_en = true;
				SysMgmt_ErrorClr( mgmt, ERROR_R_BREAK );
				SysMgmt_ErrorClr( mgmt, ERROR_L_BREAK );
			}
			break;
		default:
			break;
	}
}

void SysMgmt_ExpBuzzer( sysmgmt_t* mgmt )
{
	if( mgmt->Status.speaker_on == true ) {
		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_12, GPIO_PIN_SET);
	}
	else {
		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_12, GPIO_PIN_RESET);
	}
}

void Sys_Reset( void )
{
   void(*app_entry)(void);
   
   __disable_interrupt();
   app_entry = (void(*)(void))(0x0);
   app_entry();
}
