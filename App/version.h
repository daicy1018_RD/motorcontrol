#ifndef VERSION_H
#define	VERSION_H

/*
 * Version scheme:
 * <major>.<minor>.<dot>
 * Where:
 * <major> = Major revision (significant change that affects many or all modules)
 * <minor> = Minor revision (new features, regular releases)
 * <dot> = Dot release (error corrections, unscheduled releases)
 * Examples:
 * "0.3.0"
 * "1.0.21"
 *
 * The version number returned from the "VersionGet" function is an
 * unsigned integer in the following decimal format (not in a BCD format).
 * <major> * 10000 + <minor> * 100 + <patch>
 */
#define VER_MAJOR			0
#define VER_MINOR			20
#define VER_PATCH			0

char* GetVersionStr( void );
uint32_t GetVersion( void );


#endif	/* VERSION_H */
