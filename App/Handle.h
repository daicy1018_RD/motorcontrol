#ifndef HANDLE_H
#define	HANDLE_H

extern __fifo_t fifo_handle_x;
extern __fifo_t fifo_handle_y;

#define HANDLE_POSI_ADC_MAX			4095

typedef struct {
    uint32_t update:                1;
	uint32_t zero_ok:         		1;
	
} __handlestatus_bits;

typedef struct {
	INT32U_BIT( Status, __handlestatus_bits );
	uint32_t rawCountX;
	uint32_t rawCountY;
	uint32_t hallposX0;
	uint32_t hallposY0;
	
	uint32_t adjust_Time;
	
	int32_t  adjust_adcx[3];
	int32_t  adjust_adcy[3];
	int32_t  adc_x0;
	int32_t  adc_y0;
	
	int32_t  p_y_front;
	int32_t  p_y_back;
	int32_t  p_x_left;
	int32_t  p_x_right;
} handle_t;
__no_init extern handle_t Handle;

void Handle_Init( void );
void Handle_PosiGet( handle_t* skt );
bool Handle_ConnChk( handle_t* skt );
void Handle_ZeroChk( handle_t* skt, uint32_t hx_adc, uint32_t hy_adc );
bool Handle_ZeroChkRes( void );

#endif	/* HANDLE_H */
